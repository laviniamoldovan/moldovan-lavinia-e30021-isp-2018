package g30021.moldovan.lavinia.lab3.e1;

public class SenzorV2 {
    String nume;
    int valoare;

    void inc(){
        valoare++;
        System.out.println("Senzor "+nume+" valoare incerementata "+valoare);
    }

    int citesteValoare(){
        return valoare;
    }

    void afiseaza(){
        System.out.println("Senzor "+nume+" valoare "+valoare);
    }
}

package g30021.moldovan.lavinia.lab3.e6;

public class Masina {

    String marca;
    int vitezaCurenta;
    

    public Masina(String marca, int vitezaCurenta) {
        this.marca = marca;
        this.vitezaCurenta = vitezaCurenta;
      
    }

    void accelereaza(){
        if(vitezaCurenta<140) {
            vitezaCurenta++;
            System.out.println("Viteza curenta: "+vitezaCurenta+" Km\\h");
        }
    }

    public void franeaza() {
        vitezaCurenta--;
    }

    void afiseaza(){
        System.out.println("Masina "+marca+" viteaza = "+vitezaCurenta);
    }


}